#W folderze resources załączam ilustrację przeprowadzonych eksperymentów.

# Opis:
Początkowo uruchamiam aplikację ze składową "power" obiektu "powerService" ustawioną na 1 (ilustracja 1). 

Uruchamiam JConsole, włączam subskrypcję notyfikacji (ilustracja 2).

Zmieniam wartość składowej "power" na 2 (ilustracja 3). 

Odbieram notyfikację i sprawdzam, że zmieniło się działanie aplikacji (ilustracja 4).

Dla potęg z innych zakresów (niskie, średnie, wysokie) przychodzą odpowiednie powiadomienia i zmienia się wykładnik (sprawdziłam nie ilustrowałam).