import tokarczyk.magdalena.spark.mbean.ServiceMonitor;
import tokarczyk.magdalena.spark.mbean.ServiceStatsMonitor;
import tokarczyk.magdalena.spark.service.PowerService;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

import static spark.Spark.get;

public class Main {

    public static void main(String[] args) throws Exception {

        PowerService powerService = new PowerService();

        get("power/:arg", (req, res) -> {
            return powerService.count(req.params(":arg"));
        });

        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

        ObjectName monitorName = new ObjectName("tokarczyk.magdalena.spark:type=Server,name=PowerService");
        ServiceMonitor serviceMonitor = new ServiceMonitor(powerService);
        mbs.registerMBean(serviceMonitor, monitorName);

        ObjectName statsMonitorName = new ObjectName("tokarczyk.magdalena.spark:type=Server,name=ServiceStats");
        ServiceStatsMonitor serviceStatsMonitor = new ServiceStatsMonitor(powerService);
        mbs.registerMBean(serviceStatsMonitor, statsMonitorName);

    }

}
