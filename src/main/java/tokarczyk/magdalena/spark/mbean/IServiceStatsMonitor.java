package tokarczyk.magdalena.spark.mbean;

import javax.management.MXBean;

@MXBean
public interface IServiceStatsMonitor {
    ServiceStats getServiceStats();
}
