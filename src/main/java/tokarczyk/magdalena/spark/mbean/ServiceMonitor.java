package tokarczyk.magdalena.spark.mbean;

import tokarczyk.magdalena.spark.service.PowerService;

import javax.management.AttributeChangeNotification;
import javax.management.MBeanNotificationInfo;
import javax.management.Notification;
import javax.management.NotificationBroadcasterSupport;

public class ServiceMonitor extends NotificationBroadcasterSupport implements
        ServiceMonitorMBean {

    private PowerService powerService;
    private int sequenceNumber = 0;

    public ServiceMonitor(PowerService powerService) {
        this.powerService = powerService;
    }

    @Override
    public void setPower(double power) {

        if (power >= 1) {
            Notification notification = null;

            if (power >= 1 && power <= 5) {
                notification = new AttributeChangeNotification(this,
                        sequenceNumber++, System.currentTimeMillis(),
                        "Niska wartość potęgi", "power", "double", powerService.getPower(),
                        power);
                notification.setUserData("Niska wartość potęgi");
            } else if ( power >= 6 && power <= 10) {
                notification = new AttributeChangeNotification(this,
                        sequenceNumber++, System.currentTimeMillis(),
                        "Średnia wartość potęgi", "power", "double", powerService.getPower(),
                        power);
                notification.setUserData("Średnia wartość potęgi");
            } else {
                notification = new AttributeChangeNotification(this,
                        sequenceNumber++, System.currentTimeMillis(),
                        "Wysoka wartość potęgi", "power", "double", powerService.getPower(),
                        power);
                notification.setUserData("Wysoka wartość potęgi");
            }

            sendNotification(notification);
        }

        powerService.setPower(power);
    }

    @Override
    public MBeanNotificationInfo[] getNotificationInfo() {
        String[] types = new String[] { AttributeChangeNotification.ATTRIBUTE_CHANGE };

        String name = AttributeChangeNotification.class.getName();
        String description = "Zmiana wartosci atrybutu";
        MBeanNotificationInfo info = new MBeanNotificationInfo(types, name,
                description);
        return new MBeanNotificationInfo[] { info };
    }

    @Override
    public double getPower() {
        return powerService.getPower();
    }

}
