package tokarczyk.magdalena.spark.mbean;

public interface ServiceMonitorMBean {

    public double getPower();

    public void setPower(double power);

}


