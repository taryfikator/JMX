package tokarczyk.magdalena.spark.mbean;

import java.beans.ConstructorProperties;

public class ServiceStats {

    private double power;

    public double getPower() {
        return power;
    }
    public void setPower(double power) {
        this.power = power;
    }

    public ServiceStats() {
        super();
    }

    @ConstructorProperties({"power"})
    public ServiceStats(double power) {
        super();
        this.power = power;
    }

}