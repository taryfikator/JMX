package tokarczyk.magdalena.spark.mbean;

import tokarczyk.magdalena.spark.service.PowerService;

public class ServiceStatsMonitor implements IServiceStatsMonitor {

    private PowerService powerService;

    public ServiceStatsMonitor(PowerService powerService) {
        this.powerService = powerService;
    }

    @Override
    public ServiceStats getServiceStats() {
        return new ServiceStats(powerService.getPower());
    }


}
