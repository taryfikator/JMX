package tokarczyk.magdalena.spark.service;

public class PowerService {

    private double power = 1;

    public String count(String passedNumber) {
        double number = Double.valueOf(passedNumber);
        Double result = Math.pow(number, power);
        return result.toString();
    }

    public double getPower() {
        return power;
    }

    public void setPower(double power) {
        this.power = power;
    }

}
